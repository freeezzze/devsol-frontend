import express from 'express';
import HomeRouter from './HomeRouter';

const router = express.Router();

router.use('/', HomeRouter);

export default router;
