import express from 'express';
import { index } from '../controllers/homeController';

const homeRouter = express.Router(); // для адресов с "/"

//get запросы
homeRouter.get('/', index);

export default homeRouter;
