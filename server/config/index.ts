export const port = parseInt(process.env.PORT || '3000', 10);
export const dev = process.env.NODE_ENV !== 'production';
