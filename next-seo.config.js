export default {
  title: 'Тайтл',
  description: 'Описание',
  canonical: process.env.siteName,
  openGraph: {
    url: process.env.siteName,
    title: 'Тайтл',
    description: 'Описание',
    type: 'website',
    images: [
      {
        url: `${process.env.siteName}/og_1200x930.jpg`,
        width: 1200,
        height: 930,
        alt: '',
      },
      {
        url: `${process.env.siteName}/og_510x395.jpg`,
        width: 510,
        height: 395,
        alt: '',
      },
    ],
    site_name: 'localhost',
  },
  additionalMetaTags: [
    {
      property: 'vk:image',
      content: `${process.env.siteName}/og_510x395.jpg`,
    },
  ],
  // languageAlternate: [
  //   {
  //     hrefLang: 'de-AT',
  //     href: `${process.env.siteName}/de`,
  //   },
  //   {
  //     hrefLang: 'ru-RU',
  //     href: process.env.siteName,
  //   },
  //   {
  //     hrefLang: 'en-US',
  //     href: `${process.env.siteName}/en`,
  //   },
  // ],
  // facebook: {
  //   appId: 0, // apid facebook unic
  // },
  // twitter: {
  //   handle: '@freeezzze',
  //   site: '@localhost',
  //   cardType: 'summary',
  // },
};
