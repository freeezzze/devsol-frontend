const withPlugins = require('next-compose-plugins');
const withOptimizedImages = require('next-optimized-images');
const fonts = require('next-fonts');
const path = require('path');

const isProd = process.env.NODE_ENV === 'production';

const nextConfig = {
  crossOrigin: 'anonymous',
  exportTrailingSlash: true,
  devIndicators: {
    autoPrerender: false,
  },
  env: {
    isProduction: process.env.NODE_ENV === 'production',
    siteName: isProd ? 'https://sitename.ru' : 'http://localhost:3000',
  },
  // eslint-disable-next-line no-unused-vars
  webpack(config, options) {
    config.resolve.modules.push(path.resolve('./'));
    config.resolve.modules.push(path.resolve('./src/'));
    return config;
  },
};
module.exports = withPlugins(
  [
    fonts,
    [
      withOptimizedImages,
      {
        inlineImageLimit: 8192,
        imagesFolder: 'images',
        imagesName: '[name]-[hash].[ext]',
        handleImages: ['jpeg', 'png', 'svg', 'webp', 'ico'],
        removeOriginalExtension: true,
        optimizeImages: true,
        optimizeImagesInDev: true,
        svgo: {
          plugins: [{ removeComments: false }],
        },
        mozjpeg: {
          quality: 65,
        },
        optipng: {
          optimizationLevel: 3,
        },
        pngquant: false,
        webp: {
          preset: 'default',
          quality: 65,
        },
      },
    ],
  ],
  nextConfig
);
