import React from 'react';
import { string } from 'prop-types';
import img from 'pages/MainPage/images/main.jpg';

const ImageDynamic = () => {
  return (
      <img src={img} alt="main" />
  );
};

ImageDynamic.defaultProps = {
  className: '',
};

ImageDynamic.propTypes = {
  className: string,
};

export default ImageDynamic;
