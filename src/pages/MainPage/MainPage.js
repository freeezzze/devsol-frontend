import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useInterval from 'hooks/useInterval';
import { actions } from 'store/reducers/mainReducer';

import dynamic from 'next/dynamic';

import s from './MainPageStyles.module.scss';

const DynamicImage = dynamic(() => import('./components/ImageDynamic'), {
  loading: () => <p>Loading...</p>,
});

const MainPage = () => {
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();

  const counter = useSelector((state) => {
    return state.main.counter;
  });

  useInterval(() => {
    dispatch(actions.setCounter({ counter: counter + 1 }));
  }, 1000);

  useEffect(() => {
    console.log('counter: ', counter);
  }, [counter]);

  return (
    <div className={s.section}>
      <button
        onClick={() => {
          setShow(!show);
        }}
      >
        Click to show
      </button>
      <div className={s.image}>{show && <DynamicImage />}</div>
      <h1>{counter}</h1>
    </div>
  );
};
export default MainPage;
