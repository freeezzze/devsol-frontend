import { combineReducers } from 'redux';

import mainReducer from './mainReducer';

const appReducer = combineReducers({
  main: mainReducer,
});

const rootReducer = (state, action) => {
  if (action.type === 'LOG_OUT') {
    // eslint-disable-next-line
    state = null;
  }

  return appReducer(state, action);
};

export default rootReducer;
