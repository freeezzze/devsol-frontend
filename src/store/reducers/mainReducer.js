export const types = {
  SET_COUNTER: 'main/SET_COUNTER',
};

const init = {
  counter: 0,
};

export default function(state = init, action) {
  const { type, payload } = action;
  if (type === types.SET_COUNTER) {
    return { ...state, ...payload };
  }
  return state;
}

export const actions = {
  setCounter: (main) => ({ type: types.SET_COUNTER, payload: main }),
};
