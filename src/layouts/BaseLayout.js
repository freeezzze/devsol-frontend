import React from 'react';
import { any, string } from 'prop-types';
import { NextSeo } from 'next-seo';

import s from './BaseLayoutStyles.module.scss';

const BaseLayout = ({ children, title, origin, siteName }) => {
  return (
    <React.Fragment>
      <NextSeo
        title={title}
        canonical={origin}
        openGraph={{
          url: origin,
          title: title,
          images: [
            {
              url: `${origin}/og_1200x930.jpg`,
              width: 1200,
              height: 930,
              alt: siteName,
            },
            {
              url: `${origin}/og_510x395.jpg`,
              width: 510,
              height: 395,
              alt: siteName,
            },
          ],
          site_name: siteName,
        }}
        additionalMetaTags={[
          {
            property: 'vk:image',
            content: `${origin}/og_510x395.jpg`,
          },
        ]}
        // languageAlternate={[
        //   {
        //     hrefLang: 'de-AT',
        //     href: `${origin}/de`,
        //   },
        //   {
        //     hrefLang: 'ru-RU',
        //     href: origin,
        //   },
        //   {
        //     hrefLang: 'en-US',
        //     href: `${origin}/en`,
        //   },
        // ]}
      />
      <main>
        <div className={s.wrapperPage}>{children}</div>
      </main>
    </React.Fragment>
  );
};

BaseLayout.defaultProps = {
  title: 'default title',
};

BaseLayout.propTypes = {
  children: any,
  title: string,
};

export default BaseLayout;
