import React, { useEffect, useState } from 'react';
import BaseLayout from 'layouts/BaseLayout';
import MainPage from 'pages/MainPage';

export default function Index() {
  const [origin, setOrigin] = useState('');

  useEffect(() => {
    if (window) setOrigin(window.location.origin);
  }, []);

  return (
    <BaseLayout
      title="Main page"
      origin={origin}
      siteName={'New site from Redwest'}
    >
      <MainPage />
    </BaseLayout>
  );
}

export function getStaticProps() {
  return {
    props: {
      initialReduxState: {
        main: {
          counter: 1,
        },
      },
    },
  };
}
