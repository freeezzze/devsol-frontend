import React from 'react';
import { Provider } from 'react-redux';
import { useStore } from 'store';
// Все файлы Css/Scss без .module помещаются сюда
import 'css/index.scss';

import { DefaultSeo } from 'next-seo';
import SEO from 'next-seo.config';

export default function App({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  return (
    <Provider store={store}>
      <DefaultSeo {...SEO} />
      <Component {...pageProps} />
    </Provider>
  );
}
