// eslint-disable-next-line no-undef
System.config({
  paths: {
    'css*': './src/css*',
    'components*': './src/components*',
    'layouts*': './src/layouts*',
    'hooks*': './src/hooks*',
    'api*': './src/api*',
    'constants*': './src/constants*',
    'context*': './src/context',
    'assets*': './src/assets*',
    '*': './src/*',
  },
});
